#!/usr/bin/python3
data = {}
with open('/home/user/Загрузки/Telegram Desktop/visit_log.csv', 'r') as content:
    for line in content:
        key, value = line.strip().split(',')
        data[key] = value

with open('output.txt', 'w') as content:
    for key, value in data.items():
        content.write(f'{key}: {value}\n')
