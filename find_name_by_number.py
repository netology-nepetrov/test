documents = [
{'type': 'passport', 'number': '2207 876234', 'name': 'Василий Гупкин'},
{'type': 'invoice', 'number': '11-2', 'name': 'Геннадий Покемонов'},
{'type': 'insurance', 'number': '10006', 'name': 'Аристарх Павлов'}
]

def get_name_by_number():
    number = input("Введите номер: ")
    for doc in documents:
        if doc.get('number') == number:
            print(f" Это документ {doc.get('name')}")
            return
    print('Документ не найден')
    return

def get_command():
    command = None
    while command != "q":
        command = input("Введите команду: ")
        if command == "p":
            get_name_by_number()
            return
        if command == "q":
            print("Спасибо за использование приложения, хорошего дня!")
            return command
        else:
            print("Вы ввели не существующую команду")

print("""
====================================================================================
Добро пожаловать в консольную утилиту для определения кому принадлежит документ по

номеру! Для поиска ФИО по номеру документа введите p. Для выхода введите q.
====================================================================================
""")

while get_command() == None:
    True
