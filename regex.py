#!/usr/bin/python3
import re

def validate_car_id(car_id):
    pattern = r'^[a-zA-Z]\d{3}[a-zA-Z]{2}\d{2,3}$'
    match = re.match(pattern, car_id)
    if match:
        number = match.group(0)
        region = int(car_id[-3:]) if len(car_id) > 8 else int(car_id[-2:])
        return f'Номер {number} валиден. Регион: {region}.'
    else:
        return 'Номер не валиден.'

validate_car_id("H459HB14")
