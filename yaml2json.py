#!/usr/bin/python3
import yaml
import json

# Открываем YAML файл для чтения
with open('file.yaml', 'r', encoding='utf-8') as yaml_file:
    # Загружаем YAML данные
    yaml_data = yaml.load(yaml_file, Loader=yaml.FullLoader)

# Открываем JSON файл для записи
with open('file.json', 'w', encoding='utf-8') as json_file:
    # Записываем JSON данные
    json.dump(yaml_data, json_file, ensure_ascii=False)
