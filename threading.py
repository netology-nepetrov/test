#!/usr/bin/python3
import threading
import time

def formula1(x):
    return x^2 - x^2 + x^4 - x^5 + x + x

def formula2(x):
    return x + x

def formula3(result1, result2):
    return result1 + result2

def calculate_formula1(start, end, results):
    for i in range(start, end):
        results[i] = formula1(i)

def calculate_formula2(start, end, results):
    for i in range(start, end):
        results[i] = formula2(i)

def calculate_formula3(results1, results2):
    result = 0
    for i in range(len(results1)):
        result += formula3(results1[i], results2[i])
    return result

if __name__ == '__main__':
    start_time = time.time()

    # Вычисления для формулы 1
    results1 = [0] * 10000
    threads1 = []
    for i in range(4):
        thread = threading.Thread(target=calculate_formula1, args=(i*2500, (i+1)*2500, results1))
        threads1.append(thread)
        thread.start()

    for thread in threads1:
        thread.join()

    # Вычисления для формулы 2
    results2 = [0] * 10000
    threads2 = []
    for i in range(4):
        thread = threading.Thread(target=calculate_formula2, args=(i*2500, (i+1)*2500, results2))
        threads2.append(thread)
        thread.start()

    for thread in threads2:
        thread.join()

    # Вычисления для формулы 3
    result = calculate_formula3(results1, results2)

    print('Время выполнения формулы 1:', time.time() - start_time, 'секунд')
    start_time = time.time()

    print('Время выполнения формулы 2:', time.time() - start_time, 'секунд')
    start_time = time.time()

    print('Время выполнения формулы 3:', time.time() - start_time, 'секунд')
