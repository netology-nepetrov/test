#!/usr/bin/python3
from datetime import datetime
current_date = datetime.now()
def get_number():
    number = input("Введите номер: ")
    return number

def print_date(number):
    try:
        if int(number) == 0:
            print(current_date.strftime("%A, %B %d, %Y"))
        if int(number) == 1:
            print(current_date.strftime("%A, %d.%m.%y"))
        if int(number) == 2:
            print(current_date.strftime("%A, %d %B %Y"))
    except ValueError:
        print("Вы ввели не номер")

print("""
====================================================================================
Добро пожаловать в консольную утилиту для генерации разных форматов дат

введите 0 для сегодняшней даты в формате вида The Moscow Times: Wednesday, October 2, 2002
введите 1 для сегодняшней даты в формате вида The Guardian: Friday, 11.10.13
введите 2 для сегодняшней даты в формате вида Daily News: Thursday, 18 August 1977


Для выхода введите q.
====================================================================================
""")

number = True
while number != "q":
    number = get_number()
    if number != "q":
        print_date(number)
        
print("Хорошего дня")
